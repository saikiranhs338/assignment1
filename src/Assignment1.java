import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.ie.*;
import org.openqa.selenium.edge.*;

import java.io.IOException;

public class Assignment1 {
	public void killMethod(String s) throws IOException, InterruptedException{
		if(s.equals("chrome")) {
			Runtime.getRuntime().exec("taskkill /f /im chromedriver.exe");
			Thread.sleep(5000);
		}
		else if(s.equals("firefox")) {
			Runtime.getRuntime().exec("taskkill /f /im geckodriver.exe");
			Thread.sleep(5000);
		}
		else if(s.equals("internet explorer")) {
			Runtime.getRuntime().exec("taskkill /f /im IEDriverServer.exe");
			Thread.sleep(5000);
		}
		else if(s.equals("edge")) {
			Runtime.getRuntime().exec("taskkill /f /im MicrosoftWebDriver.exe");
			Thread.sleep(5000);
		}
	}
	public static void main(String[] args) throws IOException, InterruptedException {
		Assignment1 assign = new Assignment1();
		//chrome
		System.setProperty("webdriver.chrome.driver","D:\\ALM image\\Sel\\chromedriver.exe");
		WebDriver dr = new ChromeDriver();
		dr.get("http://www.store.demoqa.com");
		//firefox
		System.setProperty("webdriver.gecko.driver", "D:\\ALM image\\Sel\\geckodriver-v0.23.0-win64\\geckodriver.exe");
		WebDriver dr1 = new FirefoxDriver();
		dr1.get("http://www.store.demoqa.com");
		//Internet Explorer
		System.setProperty("webdriver.ie.driver", "D:\\ALM image\\Sel\\IEDriverServer_Win32_2.45.0\\IEDriverServer.exe");
		WebDriver dr2 = new InternetExplorerDriver();
		dr2.get("http://www.store.demoqa.com");
		//Microsoft Edge
		System.setProperty("webdriver.edge.driver", "D:\\ALM image\\Sel\\MicrosoftWebDriver.exe");
		WebDriver dr3 = new EdgeDriver();
		dr3.get("http://www.store.demoqa.com");
		//headless
		
		assign.killMethod("chrome");
		assign.killMethod("firefox");
		assign.killMethod("internet explorer");
		assign.killMethod("edge");
	}
}
